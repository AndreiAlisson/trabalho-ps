import java.util.*;

/*import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;*/
import java.io.*;

enum Resultado{
	correto, tenteNovamente, falhou;
}

enum Carta{
	maisTempo, maisTentativas, semBonus;
}

abstract class Problema{

	public abstract void Visualiza();
	
	public abstract Resultado Responder(String resposta);

	public abstract void Buffar();

	public abstract void Debuffar(); //Não está no diagrama

}

class CobolOrc extends Problema{
	private String codigo;
	private int linhaErro;
	public long maxTempo = 10;

	public void CriaCO(String codigo, int linhaErro){
		this.codigo = codigo;
		this.linhaErro = linhaErro;

	}

	public void Visualiza(){
		System.out.printf("%n%s%n", this.codigo);

	}
	public Resultado Responder(String resposta){
		
		int resp = Integer.parseInt(resposta);

		if(resp == this.linhaErro)
			return Resultado.correto;
		else{
			return Resultado.falhou;
		}
	}

	public void Buffar(){
		
		this.maxTempo *= 2;

	}

	//Não está no diagrama
	public void Debuffar(){

		this.maxTempo /= 2;
	}

}

class Cadeado extends Problema{
	private String descricao;
	public int entrada; //int -> 1 instancia de teste de entrada
	public int saida;  //int -> 1 instancia de teste de saida
	public int maxTentativas;
	public int tentativas;

	public void CriaProblema(String enunciado, int entrada, int saida){

		this.descricao = enunciado;
		this.maxTentativas = 3;
		this.tentativas = 0;
		this.entrada = entrada;
		this.saida = saida;

	}

	public void Visualiza(){
		
		System.out.printf("%n%s%n", this.descricao);

	}
	public Resultado Responder(String resposta){

		this.tentativas++;

		if(resposta.length() > 0){
			String saida = resposta.substring(0, (resposta.length()-1));
			//System.out.printf("resposta: %s%n", resposta);
			if(Integer.parseInt(saida) == this.saida)
				return Resultado.correto;
			else
				return Resultado.tenteNovamente;
		}

		return Resultado.tenteNovamente;

	}

	public void Buffar(){
		
		this.maxTentativas++;

	}

	//Não está no diagrama
	public void Debuffar(){

		this.maxTentativas--;

	}

}

class Sala{
	private int id;
	private boolean temPF;
	private boolean temCO;

	public Cadeado cad;
	public CobolOrc corc;

	//Não está no diagrama
	public void CriaSala(int id, String enunciado, int entrada, int saida, boolean co, boolean pf, String codigo, int linhaErro){

		this.cad = new Cadeado();
		this.cad.CriaProblema(enunciado, entrada, saida);
		this.id = id;
		this.temPF = pf;
		this.temCO = co;

		if(temCO == true){
			this.corc = new CobolOrc();
			this.corc.CriaCO(codigo, linhaErro);
		}

	}

	public int GetID(){

		return this.id;

	}

	public boolean TemPF(){

		return this.temPF;

	}

	public boolean TemCO(){

		return this.temCO;

	}

	public Resultado EnfrentaCO(){

		this.corc.Visualiza();

		System.out.printf("%nCaro guerreiro, digite o número da linha no qual você acredita que há um erro: ");

		Resultado res;

		long start = System.currentTimeMillis();

		Scanner scanner = new Scanner(System.in);
    	String resposta = scanner.nextLine();

    	long finish = System.currentTimeMillis();
    	long total = finish - start;	//Diferença entre o tempo final e inicial em ms

    	if((total/1000) <= this.corc.maxTempo){
			res = this.corc.Responder(resposta);

			if(res == Resultado.correto)
				return Resultado.correto;
    	}
    	else
    		System.out.printf("Tempo limite excedido.%n");

		return Resultado.falhou;

	}

	public String ExecutaComando(String command){

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
	                        new BufferedReader(new InputStreamReader(p.getInputStream()));

	                    String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}

	public Resultado ResolveCad() throws IOException{

		this.cad.Visualiza();

		String resposta = "", rTmp;
		char cTmp = '\u0000';
		Scanner scanner;

		Resultado res;

		System.out.printf("%nNota: Você deve escrever linha por linha e teclar enter. Dê um print no resultado ao final do código e após encerrar seu programa, digite 0 e tecle enter%n");

		for(int i = 0; i < this.cad.maxTentativas; i++){

			//Cria arquivo com input do jogador codigo.pas
			//Jogador escreve linha a linha do código e no final escreve um print do resultado.
			//Na última linha o jogador deve inserir um '0' indicando o fim de sua escrita
    		FileWriter arq = new FileWriter("codigo.pas");
		    PrintWriter gravarArq = new PrintWriter(arq);
		 	
			do{
				scanner = new Scanner(System.in);
				rTmp = scanner.nextLine();
				if(rTmp.length() > 0){
					cTmp = rTmp.charAt(0);
					if(cTmp != '0'){
		     			gravarArq.printf("%s%n", rTmp);
		     		}
				}

			}while(cTmp != '0');

			gravarArq.close();
			arq.close();

			//Remove qualquer executável que tenha o nome codigo
			String domainName = "codigo codigo.o";
			String remove = "rm " + domainName;
			this.ExecutaComando(remove);

			//chama compilador e compila o codigo.pas
			domainName = "codigo.pas";			
			String compila = "fpc " + domainName;	
			this.ExecutaComando(compila);

			//Testa se o programa compilou pra prosseguir
			domainName = "codigo";
			String testaComp = "ls " + domainName;	
			String str = this.ExecutaComando(testaComp);
			char tmp;

			if(str.length() > 0)
				tmp = str.charAt(0);
			else
				tmp = '\u0000';

			if(tmp == 'c'){
				System.out.printf("Programa compilou! (:%n");

				//Cria arquivo de input com a entrada
				FileWriter input = new FileWriter("input.txt");
		    	PrintWriter gravarInput = new PrintWriter(input);
		    	gravarInput.printf("%d", this.cad.entrada);
		    	gravarInput.close();
		    	input.close();
		    	//System.out.printf("fechou arquivo%n");

		    	//Executa o codigo e coloca a saída em resposta
				domainName = "codigo";// < input.txt";/**********colocar input trava a execução**********/
				String executa = "./" + domainName;
				System.out.printf("comando: %s%n", executa);
				resposta = this.ExecutaComando(executa);

			}
			else{
				System.out.printf("Programa não compilou!%n");
				resposta = "";
			}

			//System.out.printf("Resposta: %s%n", resposta);
			res = cad.Responder(resposta);

			if(res == Resultado.correto){
				this.ExecutaComando(remove); //Remove o programa compilado do diretório
				return Resultado.correto;
			}

		if(i+1 < this.cad.maxTentativas)
			System.out.printf("Programa incorreto, tente novamente! Restam %d tentativas.%n", this.cad.maxTentativas - this.cad.tentativas);
		else
			this.ExecutaComando(remove); //Remove o programa compilado do diretório

		}

		this.cad.tentativas = 0;
		return Resultado.falhou;

	}

}

class ListaSalas{
	private ArrayList <Sala> listaSalas;
	public int numSalas;

	private Sala s;

	//Não está no diagrama
	public void CriaLista(){
		this.listaSalas = new ArrayList<Sala>();
		
	}

	//Não está no diagrama
	public void SetList(Sala s){

		this.numSalas++;
		this.listaSalas.add(s);

	}

	public Sala GetSala(int id){

		for(int i = 0; i < this.numSalas; i++)
			if(id == this.listaSalas.get(i).GetID())
				return this.listaSalas.get(i);

		return null;

	}

}

class Jogador{
	private String nome;
	public int vidas;
	private ArrayList <Sala> progresso;
	private int counterProgresso;
	private int counterCartas;

	//Não está no diagrama
	public void CriaJogador(String nome){

		this.vidas = 3;
		this.nome = nome;
		this.counterCartas = 0;
		this.progresso = new ArrayList<Sala>();

	}

	//Não está no diagrama
	public void AdicionaProgresso(Sala s){
		this.counterProgresso++;
		this.progresso.add(s);
	}

	//Não está no diagrama
	public void ResetProgress(){

		for(int i = this.counterProgresso - 1; i >= 0; i--)
			this.progresso.remove(i);

		this.counterProgresso = 0;
		this.counterCartas = 0;

	}

	public void VisualizaCartas(){
		System.out.printf("Caro guerreiro, você possui %d cartas disponíveis!%n", counterCartas);

	}

	public void VisualizaProgresso(){

		if(this.counterProgresso == 0)
			System.out.printf("%nVocê não resolveu nenhum problema.%n");
		else
			for(int i = 0; i < this.counterProgresso; i++)
				System.out.printf("Problema nº %d resolvido com %d tentativas!%n", this.progresso.get(i).GetID() + 1, this.progresso.get(i).cad.tentativas);

	}

	public boolean UtilizaCarta(){

		if(this.counterCartas > 0){
			this.counterCartas--;
			return true;		
		}

		return false;

	}

	public void AddCarta(){

		this.counterCartas++;

	}

}

class Jogo{

	private Sala salaAtual;
	private Carta cartaEmUso;

	private ListaSalas listS;
	private Jogador j;

	//Não está no diagrama
	public void Joga() throws IOException{

		//Cria as salas manualmente
		//Formato dos parâmetros: CriaSala(idsala, enunciado, entrada esperada, saida esperada, temCO?, temPF?, codigo pro CO, linha do erro)
		listS = new ListaSalas();
		listS.CriaLista();

		Sala s0 = new Sala();
		s0.CriaSala(0, "Crie um programa que imprima a potencia quadrada de 4.", 4, 16, true, false, "Program p;\nbegin\nwriteln('10')\nend;", 4);
		listS.SetList(s0);

		Sala s1 = new Sala();
		s1.CriaSala(1, "Crie um programa que imprima o fatorial de 5.", 5, 120, true, false, "Program p;\nvar x: inteiro;\nwriteln(x)\nend.", 2);
		listS.SetList(s1);

		Sala s2 = new Sala();
		s2.CriaSala(2, "Crie um programa que imprima a raíz quadrada de 100.", 100, 10, false, false, "", -1);
		listS.SetList(s2);

		Sala s3 = new Sala();
		s3.CriaSala(3, "", 0, 0, false, true, "", -1);
		listS.SetList(s3);

		this.cartaEmUso = Carta.semBonus;

		char str, str2;
		Resultado res;

		System.out.printf("%nCaro guerreiro, digite o seu apelido: ");
		Scanner nameInput = new Scanner(System.in);
    	String name = nameInput.nextLine();

    	j = new Jogador();
    	j.CriaJogador(name);

   		System.out.printf("%nSeja bem vindo, %s!%n", name);

		System.out.printf("%n%s é um(a) explorador(a) e está em busca do 'PerfectCode'. O PerfectCode é um artefato mágico que faz com que todos os códigos feitos por quem o possui serem perfeitos. O PerfectCode foi escondido no Castelo Fortran pelo grande bruxo POG. O objetivo é encontrar o PerfectCode seguindo as pistas e enfrentando os mostros COBOLORCs que protegem o artefato.%n", name);
		System.out.printf("%nPara entrar no castelo e seguir com sua aventura em busca do PerfectCode, pressione a tecla 's'.%n");

		do{
			Scanner scanner = new Scanner(System.in);
			str = scanner.next().charAt(0);
		}while(str != 's');

		for(int i = 0; i < listS.numSalas; i++){ 

			do{
				System.out.printf("%nPressione a tecla 'a' para abrir a próxima porta.%nPressione a tecla 'c' para ver suas cartas.%nPressione a tecla 'u' para utilizar uma carta.%nPressione a tecla 'p' para ver seu progresso no jogo.%n");
				Scanner scanner = new Scanner(System.in);
				str = scanner.next().charAt(0);

				if(str == 'c'){
					j.VisualizaCartas();
				}
				else if(str == 'u'){
					if(j.UtilizaCarta() == true){
						
						do{
							System.out.printf("%nPressione '1' para utilizar o bônus que dá mais tentativas na próxima porta ou pressione '2' para utilizar o bonus que dobra o tempo para o próximo monstro.%n");
							scanner = new Scanner(System.in);
							str2 = scanner.next().charAt(0);
						}while(str2 != '1' && str2 != '2');

						if(str2 == '1'){
							this.cartaEmUso = Carta.maisTentativas;
							System.out.printf("%nCarta maisTentativas utilizada com sucesso!%n");
						}
						else if(str2 == '2'){
							this.cartaEmUso = Carta.maisTempo;
							System.out.printf("%nCarta maisTempo utilizada com sucesso!%n");
						}

					}
					else
						System.out.printf("%nInfelizmente você não possui cartas para utilizar.%n");
				}
				else if(str == 'p')
					j.VisualizaProgresso();

			}while(str != 'a');


			res = this.ExploraSala(i);

			if(res == Resultado.falhou){
				j.vidas--;
				if(j.vidas > 0)
					System.out.printf("%nVocê falhou dessa vez, guerreiro, e deve retornar ao inicio. Tenha mais cuidado! Restam %d vidas%n", j.vidas);
				else
					System.out.printf("Acabaram as suas vidas, guerreiro.%n");
				j.ResetProgress();	
				i = -1;

				if(j.vidas == 0){
					System.out.printf("%nFim de jogo, você falhou miseravelmente ):%n");
					System.exit(0);
				}

			}		
	
		}

	}

	private Resultado ExploraSala(int id) throws IOException{

		char str;
		Resultado res;

		salaAtual = this.listS.GetSala(id);

		if(salaAtual.TemCO() == true){

			System.out.printf("%nOpa! Parece que surgiu um CobolOrc tentando impedí-lo. Pressione a tecla 'e' para enfrentá-lo!%n");

			do{
				Scanner scanner = new Scanner(System.in);
				str = scanner.next().charAt(0);
			}while(str != 'e');

			if(this.cartaEmUso == Carta.maisTempo){
				salaAtual.corc.Buffar();  //Usar sala pela lista?
				this.cartaEmUso = Carta.semBonus;
			}

			res = salaAtual.EnfrentaCO();

			if(this.cartaEmUso == Carta.maisTempo){
				salaAtual.corc.Debuffar();
				this.cartaEmUso = Carta.semBonus;
			}

			if(res == Resultado.falhou){
				return res;
			}
			else if(res == Resultado.correto){

				System.out.printf("%nParabéns, você derrotou o CobolOrc (:%n");
				j.AddCarta();
			}
			
		}

		if(salaAtual.TemPF() == true){
			System.out.printf("%nPerfectCode encontrado, fim de jogo!!%n");
			System.exit(0);
		}
		else
			System.out.printf("%nParece que o PerfectCode não está nesta sala ):%n%nMas não desista, logo a frente há uma próxima porta! Pressione a tecla 'r' para resolver um problema em pascal e desbloquear a próxima porta.%n");

		do{
			Scanner scanner = new Scanner(System.in);
			str = scanner.next().charAt(0);
		}while(str != 'r');

		if(this.cartaEmUso == Carta.maisTentativas){
			salaAtual.cad.Buffar();  //Usar sala pela lista?
		}

		res = salaAtual.ResolveCad();

		if(this.cartaEmUso == Carta.maisTentativas){
			salaAtual.cad.Debuffar();
			this.cartaEmUso = Carta.semBonus;
		}

		if(res == Resultado.falhou){
			return res;
		}
		else if(res == Resultado.correto){
			System.out.printf("%nParabéns! você resolveu o problema e desbloqueou a próxima porta!%n");
			j.AdicionaProgresso(salaAtual); //usar sala pela lista?
		}

		return Resultado.correto;

	}
}

public class Programa{

	public static void main(String args[]) throws IOException{

		Jogo jogo = new Jogo();

		jogo.Joga();
	
	}

}