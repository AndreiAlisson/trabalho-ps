
->Como adicionar novos problemas:
  Criando novas instâncias de Sala e inserindo-as na lista no início do método Joga(), com o formato abaixo:

  CriaSala(idsala, enunciado, entrada esperada, saida esperada, temCO?, temPF?, Codigo do co aqui, linha do erro)

    -Formato do codigo do co:"linha1\nlinha2\nlinha3\n...linhaX"

->Como responder um problema:
  Insira linha por linha do código no terminal, teclando enter após o fim de cada linha. Faça um print do resultado no fim do seu programa. Após o términio do seu código, digite 0 para sair do laço de escrita.
  OBS: O programa não funciona com programas em pascal que exigem leitura de entrada, os códigos devem ser limitados a escrita de saídas somente, como definidos nos enunciados.